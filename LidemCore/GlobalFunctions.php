<?php
namespace LidemCore;

abstract class GlobalFunctions
{
    public static function cleanData ($data) : string
    {
        return htmlspecialchars(
            strip_tags(
                trim($data)
            )
        );
    }

    public static function  toSlug($data) : string
    {
        //mettre tout en minuscule + remplacer les espaces par des _ + enlever les accents
        $search  = array(' ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ');
        $replace = array('_', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y');
        str_replace($search, $replace, $data);

        return self::cleanData($data);
    }


    /**
     * Permet de vérifier si une date est bien au format year / month / day
     * @param string $date
     * @return bool
     */
    public function dateIsValid(string $date) :bool
    {
        if(empty($date)) return false;
        $arr = explode('-',$date);
        return checkdate($arr[1],$arr[2], $arr[0]);
    }
}
