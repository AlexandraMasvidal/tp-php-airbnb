<?php

namespace LidemCore;

//Un trait permet de gérer une portion de code directement utilisable dans plusieurs  classes sans notion de
//hiérarchie de sorte que self représente ici la classe qui utilise le trait
//si on était dans un contexte hiérarchique le mot self désignerait la classe dans laquelle il apparaît
//un trait s'utilise typiquement lorsque l'on veut créer un framework et séparer deux parties
// d'une même logique (une dans le framework et une dans l'app).

trait RepositoryManagerTrait
{
    private static ?self $rm_instance = null;

    public static function getRM() : self
    {
        $class = get_called_class();
        if(is_null(self::$rm_instance)) self::$rm_instance = new self();
        return self::$rm_instance;
    }

    protected function __construct()
    {

    }
    private function __clone() {}
    private function __wakeup() {}
}