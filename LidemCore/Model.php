<?php

namespace LidemCore;

use LidemCore\Database\Database;
use \PDO;

abstract class Model // = hydrator
{
    public int $id;

    //protected static PDO $pdo_instance = Database::getPDO()

    public function __construct( array $data_row = [])
    {
        //$model = get_called_class();
        foreach( $data_row as $column => $value) {
            if( ! property_exists($this, $column)) continue;

            $this->$column = $value;
        }
    }
}