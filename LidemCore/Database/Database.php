<?php

namespace LidemCore\Database;
use \PDO;

class Database // = singleton "trafiqué" qui retourne un objet PDO
{
    private static ?PDO $pdo_instance = null;

    private const PDO_OPTIONS = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ];

    public static function getPDO( DatabaseConfigInterface $config) : PDO
    {
        if (is_null(self::$pdo_instance)) {
            $dsn = sprintf( 'mysql:dbname=%s;host=%s', $config->getName(), $config->getHost() );
            self::$pdo_instance = new PDO($dsn, $config->getUser(), $config->getPass(), self::PDO_OPTIONS);
        }
        return self::$pdo_instance;
    }

    private function __construct() {}
    private function __clone()  {}
    private function __wakeup() {}
}