<?php

namespace LidemCore\Http;

class Route
{
    public string $url; //URL qui caractérise la route
    public string $method = '';
    public array $action;

    public function __construct( string $str_request, array $action )
    {
        $arr_route = explode('|', $str_request );

        if( count($arr_route) < 2) {
            $this->url = strtolower($arr_route[0]);
        }
        else
        {
            $this->url = strtolower($arr_route[1]);
            $this->method = $arr_route[0];
        }
        $this->action = $action;
    }


}