<?php

namespace LidemCore\Http;
use \Throwable;

class Router
{
    private array $routes = [];

    public function __construct()  { }

    public function registerRoute( string $str_request, array $action ) : self
    {
        $route = new Route($str_request, $action);
        $this->routes[] = $route;

        return $this;
    }

    public function start() : void
    {
        $requested_route = null;
        $requested_url = $_SERVER['REDIRECT_URL'] ?? '/';
        $requested_method = strtolower($_SERVER[ 'REQUEST_METHOD']);

        foreach($this->routes as $route) {
            if ($route->url !== $requested_url) continue;
            if(!empty( $route->method )&& $route->method !== $requested_method) continue;

            $requested_route = $route;
            break;
        }

        if(is_null( $requested_route )) throw new RouteNotFoundException($requested_url);

        $is_valid = true;

        if(count( $requested_route->action) < 2) $is_valid = false;
        else if ( !class_exists($requested_route->action[0])) $is_valid = false;
        else if ( !method_exists($requested_route->action[0],$requested_route->action[1])) $is_valid = false;

        if (!$is_valid ) throw new InvalidRouteDataException( $requested_route->action );

        $page_class = $requested_route->action[0];
        $page_instance = new $page_class();


        // TODO : gérer le passage des arguments à l'action
        try {
            call_user_func_array([$page_instance, $requested_route->action[1]], []);
        }
        catch( Throwable $e) {
            throw new InvalidRouteDataException($requested_route->action, $e);
        }
    }
}