<?php
global $user
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@200;300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/css/reset.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <title><?php echo $title_tag ?></title>
</head>

<body>
    <header>
<!--     <a href="/locations">
            <div class="logo">
                Air BnBis
            </div>
        </a>-->
        <div class="user-tag">
            Bienvenue <?php echo $user->pseudo === '' ? 'Visiteur' : $user->pseudo ?>
        </div>
        <div class="menu-container">
        <?php
        //$user = $_SESSION['user-mode'];
        global $user_mode;

        switch ($user_mode) {
            case REGISTERED:
/*                echo '<form action="" method="post" class="searchbar">';
                echo     '<input type="text" placeholder="Où ?">';
                echo     '<input type="date" placeholder="de">';
                echo     '<input type="date" placeholder="à">';
                echo     '<input type="number" placeholder="Nombre de voyageurs">';
                echo     '<input type="submit" value="Rechercher">';
                echo '</form>';*/
                echo '<div class="menu">';
                //echo    '<a href="#">Recherche détaillée</a>';
                echo    '<a href="/locations">Locations</a>';
                echo    '<a href="/mes-reservations">Mes Réservations</a>';
                echo     '<a href="/mon-profil">Mon profil</a>';
                echo     '<a href="/deconnexion">Déconnexion</a>';
                echo '</div>';
                break;
            case HOST:
                echo '<div class="menu">';
                echo    '<a href="/locations">Mes annonces</a>';
                echo    '<a href="/ajouter-un-logement">Ajouter un hébergement</a>';
                echo    '<a href="/mes-reservations">Mes Réservations</a>';
                //echo    '<a href="/ajouter-un-logement">Ajouter une annonce</a>';
                echo    '<a href="/mon-profil">Mon Profil</a>';
                echo     '<a href="/deconnexion">Déconnexion</a>';
                echo '</div>';
                break;
            default:
                break;
        }

        ?>
        </div>
    </header>
    <main>
