
<div class="h1">
    Mes réservations
</div>

<div class="container">
    <div class="bookings-container">
        <div class="booking bar">
            <div class="booking-title">Location</div>
            <div class="booking-date">Date d'arrivée - Date de départ</div>
        </div>

        <?php foreach($bookings as $booking): ?>

            <div class="booking">
                <div class="booking-title"><?php echo $booking->title ?></div>
                <div class="booking-date">Du : <?php echo $booking->check_in ?> au <?php echo $booking->check_out ?></div>
                <a class="booking-details" href="/hebergement/<?php echo $booking->rental_id ?>">Détails du logement</a>
            </div>
        <?php endforeach; ?>
    </div>
</div>


