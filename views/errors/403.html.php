<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0">
        <title>Server error</title>
    </head>
    <body>
        <h1>403 - Forbidden</h1>
        <p>You shall not pass !</p>
        <hr>
        <em>mon-super-site-mvc.com - &copy;2022</em>
    </body>
</html>