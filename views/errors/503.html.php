<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0">
    <title>Server error</title>
</head>
<body>
<h1>503 - Service Unavailable</h1>
<p>Service temporary unavailable</p>
<hr>
<em>mon-super-site-mvc.com - &copy;2022</em>
</body>
</html>