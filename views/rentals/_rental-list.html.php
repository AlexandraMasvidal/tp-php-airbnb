<?php
$title = '';
if($_SESSION['user']->type === HOST) $title = 'Liste de mes annonces';
if($_SESSION['user']->type === REGISTERED) $title = 'Liste des locations';
//var_dump($_SESSION);

?>

<div class="error-msg">
    <?php
    echo $_SESSION['form-error'];
    $_SESSION['form-error'] = '';
    ?>
</div>
<div>

    <h1 class="h1"><?php echo $title ?></h1>

    <!--S'il n'y a aucun hébergement à afficher (par ex si le proprio n'a pas encore enregistré de lgt-->
    <?php if(empty($rentals)): ?>
        <div>Aucun hébergement enregistré</div>
    <?php endif; ?>

    <!--Sinon, affichage de la liste de logements dans une ul-->
    <?php if(!empty($rentals)): ?>
        <div class="rentals-list">

            <?php foreach($rentals as $rental): ?>

                <a class="rental-item" href="/hebergement/<?php echo $rental->id ?>">
                    <div class="rigth">
                        <div class="rental-title">
                            <h2><?php echo $rental->title ?> </h2>
                        </div>
                        <div class="rental-location"> <?php echo $rental->city ?> - <?php echo $rental->country ?>  </div>
                        <div class="rental-info-list">
                            <div class="rental-info-item">Prix : <?php echo $rental->price ?> € / nuitée</div>
                            <div class="rental-info-item">Capacité : <?php echo $rental->capacity ?> personnes</div>
                            <div class="rental-info-item">Type : <?php echo $rental->type ?></div>
                            <div class="picto"><!--TODO:ici liste des picto en fonction des equipement--></div>
                        </div>
                    </div>
                    <div class="left"></div>
                </a>
            <?php endforeach; ?>

        </div>
    <?php endif; ?>

    <!--sinon affichage de la liste des logements :-->

</div>

