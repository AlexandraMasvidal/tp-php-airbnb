
<div class="container">
    <div class="error-msg">
        <?php echo $_SESSION['form-error'];
        $_SESSION['form-error'] = '';
        ?>

    </div>
    <h1>Ajouter un nouvel hébergement</h1>
    <form class="new-rental" action="/register-informations" method="post">

        <!--Type d'hébergement-->
        <div class="form-group-container">
            <div class="form-group">
                <div class="h3">Type d'hébergement :</div>
                <div class="place-type-choices">
                    <label for="">
                        <span class="label-name">Chambre partagée :</span>
                        <input type="radio" name="type" value="1">
                    </label>
                    <label for="">
                        <span class="label-name">Chambre privée:</span>
                        <input type="radio" name="type" value="2">
                    </label>
                    <label for="">
                        <span class="label-name">Logement entier :</span>
                        <input type="radio" name="type" value="3" checked>
                    </label>
                </div>
            </div>

        </div>

        <!--Nombre de couchages-->
        <div class="form-group-container">
            <div class="form-group">
                <label for="capacity">
                    <span class="h3 inline">Nombre de couchages :</span>
                    <input type="text" name="capacity" id="capacity">
                </label>
            </div>
        </div>


        <!--Surface-->
        <div class="form-group-container">
            <div class="form-group">
                <label for="surface">
                    <span class="h3 inline">Surface :</span>
                    <input type="text" name="surface" id="surface"> m²
                </label>
            </div>
        </div>

        <!--Adresse-->
        <div class="form-group-container">
            <div class="form-group">
                <label for="country">
                    <span class="h3 inline">Pays :</span>
                    <input type="text" name="country" id="country">
                </label>
            </div>
            <div class="form-group">
                <label for="city">
                    <span class="h3 inline">Ville :</span>
                    <input type="text" name="city" id="city">
                </label>
            </div>
        </div>


        <!--Equipements-->
        <div class="form-group-container">
            <div class="equipements">
                <span class="h3">Equipements :</span>
                <!--Depuis le tableau qui contient l'ensemble des champs de la table equipements-->
                <?php foreach($eqpts as $item) {
                    echo '<div class="form-group">';
                    echo '<input id="'.$item->label.'" type=checkbox name="equipments[]" value="' . $item->id . '"/>';
                    echo '<label for="'.$item->label.'">' . $item->label;
                    echo '</label>';
                    echo '</div>';
                }
                ?>
            </div>
        </div>


        <!--Prix-->
        <div class="form-group-container">
            <div class="form-group">
                <label for="price">
                    <span class="h3">Prix de la nuitée :</span>
                    <input type="text" name="price" id="price"> €
                </label>
            </div>
        </div>


        <!--Titre-->
        <div class="form-group-container">
            <div class="form-group block">
                <label for="title">
                    <span class="h3">Choisissez un titre pour votre annonce :</span>
                    <input type="text" name="title" id="title">
                </label>
            </div>
        </div>


        <!--Description-->
        <div class="form-group-container">
            <div class="form-group block">
                <label for="description">
                    <span class="h3">Renseignez le contenu de votre annonce :</span>
                    <textarea name="description" id="description" cols="30" rows="10"></textarea>
                </label>
            </div>
        </div>

        <input class="button" type="submit" value="Publier cette annonce">
    </form>
</div>



<!--
titre
description
surface
adresse
type
prix
equipements
-->