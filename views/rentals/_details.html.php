<?php
global $user;
//var_dump($user);
?>

<h1 class="h1">Détails de l'hébergement</h1>
<div class="place-details-container">
    <div class="place-details-info">

        <div class="details-title">
            <h2 class="h2"><?php echo $rental->title ?></h2>
            <div><?php echo $rental->city ?> - <?php echo $rental->country ?></div>
            <div><?php echo $rental->type ?></div>
            <div><?php echo $rental->capacity ?>P</div>
            <div class="price"><?php echo $rental->price ?> €/nuit</div>
        </div>

        <div class="main-info">
            <div class="h3">Caractéristiques principales :</div>
            <div>Localisation : <?php echo $rental->city ?> - <?php echo $rental->country ?></div>
            <div>Surface : <?php echo $rental->surface ?> m²</div>
            <div>Type de logement : <?php echo $rental->type ?></div>
            <div>Capacité maximale : <?php echo $rental->capacity ?> personnes</div>
        </div>

        <div class="equipements">
            <div class="h3">Liste des équipements : </div>
            <ul>
            <?php foreach($rental->equipments as $item)
            {
                echo '<li>' . $item->label . '</li>';
            }
            ?>
        </ul>
        </div>

        <div class="description">
            <div class="h3">Descriptif détaillé :</div>
            <?php echo $rental->description ?>
        </div>
    </div>

    <?php if($user->type !== 3): ?>
    <div class="reservation-container">

        <form action="/new-booking" method="post">
            <div class="error-msg">
                <?php
                echo $_SESSION['form-error'];
                $_SESSION['form-error'] = '';
                ?>
            </div>
            <div class="h3">Réserver en un clic :</div>
            <div>Choissisez vos dates :</div>
            <label for="checkin">
                Du:
                <input type="date" name="check_in" id="checkin">
            </label>
            <label for="checkout">
                Au:
                <input type="date" name="check_out" id="checkout">
            </label>
            <div>
                Prix-total : <?php echo $rental->price ?> €
            </div>
            <input type="hidden" name="rental_id" value="<?php echo $rental->id ?>">
            <button class="button" type="submit" value="Réserver">Réserver !</button>
        </form>
    </div>
    <?php endif; ?>
</div>



