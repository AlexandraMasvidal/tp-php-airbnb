<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Handlee&family=Jost:wght@200;300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/css/reset.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <title>AirBnBiss</title>
</head>
<body>

<div class="home">

    <div class="home-content">
        <div class="home-title">Air BnBis</div>
        <div class="home-btn">
            <a href="/register" class="button">Inscription</a>
            <a href="/login" class="button">Connection</a>
        </div>
    </div>

</div>


</body>
</html>