<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Handlee&family=Jost:wght@200;300;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/css/reset.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <title>Connection</title>
</head>
    <body>
        <div class="home-form-container">
            <div class="bg-left connexion"></div>
            <form action="/connexion-user" method="post" class="register-form">
                <div class="user-msg">
                    <?php echo $_SESSION['user-msg'] ?? ''; $_SESSION['user-msg'] = ''; ?>
                </div>
                <div class="error-msg"><?php
                    //var_dump($_SESSION);
                    echo $_SESSION['form-error'];
                    $_SESSION['form-error'] = '';
                    ?>
                </div>
                <div class="form-title">Se connecter</div>
                <label for="email">
                    <span>Email</span>
                    <input type="text" name="email">
                </label>
                <label for="pass">
                    <span>Mot de passe</span>
                    <input type="password" name="pass">
                </label>

                <input class="button" type="submit" value="Se connecter">
                <div class="redirection-sentence">Si vous n'avez pas de compte, <a class="redirection-link" href="/register">cliquez ici pour vous enregister et accéder à toutes nos offres</a></div>
            </form>
        </div>

    </body>
</html>
