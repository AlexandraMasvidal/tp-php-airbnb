<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1.0">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Handlee&family=Jost:wght@200;300;400&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="../../assets/css/reset.css">
        <link rel="stylesheet" href="../../assets/css/style.css">
        <title>Inscription</title>
    </head>
    <body>

        <div class="home-form-container">
            <div class="bg-left register"></div>
            <form action="/register-user" method="post" class="register-form">
                <div class="error-msg"><?php
                    //var_dump($_SESSION);
                    echo $_SESSION['form-error'];
                    $_SESSION['form-error'] = '';
                    ?>
                </div>
                <div class="form-title">S'inscrire</div>
                <label for="pseudo">
                    <span>Pseudo</span>
                    <input type="text" name="pseudo" value="">
                </label>
                <label for="email">
                    <span>Email</span>
                    <input type="email" name="email">
                </label>
                <label for="email">
                    <span>Email</span>
                    <input type="email" name="emailbis">
                </label>
                <label for="pass">
                    <span>Mot de passe</span>
                    <input type="password" name="pass">
                </label>
                <label for="check-pass">
                    <span>Confirmez votre mot de passe</span>
                    <input type="password" name="check-pass">
                </label>
                <div class="user-mode-choice">
                    <span class="form-sub-title">Je souhaite :</span>
                    <label for="host">
                        <span>Mettre un hébergement en location</span>
                        <input type="radio" name="type" id="host" value="3">
                    </label>
                    <label for="squatter">
                        <span>Rechercher une location pour les vacances</span>
                        <input type="radio" name="type" id="squatter" value="2" checked>
                    </label>
                </div>
                <input class="button" type="submit" value="S'inscrire">
            </form>
        </div>


    </body>
</html>
