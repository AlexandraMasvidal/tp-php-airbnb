<?php

use App\App;

define('DS', DIRECTORY_SEPARATOR);
define('PATH_ROOT', __DIR__ . DS);

//user-modes for checking the variable 'user-mode' created in App.php->start()
const VISITOR    = 1;
const REGISTERED = 2;
const HOST       = 3;

//rental type
const SHARED_ROOM   = 1;
const PRIVATE_ROOM  = 2;
const ENTIRE_PLACE  = 3;




// Autoload : celui du composer
require_once PATH_ROOT . 'vendor/autoload.php';

App::getApp()->start();