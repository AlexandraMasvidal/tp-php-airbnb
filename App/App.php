<?php
namespace App;

    use App\Controller\BookingsController;
    use App\Controller\EquipmentsController;
    use App\Controller\PageController;
    use App\Controller\RegisterController;
    use App\Controller\RentalController;
    use App\Controller\UserController;
    use LidemCore\Database\DatabaseConfigInterface;
    use LidemCore\View;
    use MiladRahimi\PhpRouter\Exceptions\InvalidCallableException;
    use MiladRahimi\PhpRouter\Exceptions\RouteNotFoundException;
    use MiladRahimi\PhpRouter\Router;


    class App implements  DatabaseConfigInterface
    {

        private const DB_HOST   = 'database';
        private const DB_NAME   = 'lamp';
        private const DB_USER   = 'lamp';
        private const DB_PASS   = 'lamp';

        private static ?self $instance = null;

        private Router $router;
        private function __construct() {
            $this->router = Router::create();
        }

        // "commnunique" les infos du serveur de DB du projet à la class Database qui sera réutilisable grâce à
        // l'implémentation de l'interface DatabaseConfig (elle aussi dans le coeur du projet)
        public function getHost(): string
        {
            return self::DB_HOST;
        }
        public function getName(): string
        {
            return self::DB_NAME;
        }
        public function getUser(): string
        {
            return self::DB_USER;
        }
        public function getPass(): string
        {
            return self::DB_PASS;
        }

        private function __clone(){}
        private function __wakeup(){}

        public static function getApp() :self
        {
            if (is_null(self::$instance)) self::$instance = new self();
            return self::$instance;
        }

        public function start() :void
        {
            global $user_mode;
            global $user;
            global $equipements;

            session_start();
            $user_mode = $_SESSION['user-mode'] ?? '';
            //$user_mode = 2;
            $user      = $_SESSION['user'] ?? '';
            //$_SESSION['form-error'] = '';

            $this->registerAllRoutes();
            $this->startRouter();
        }

        private function registerAllRoutes(): void
        {
            //Déclaration des patterns pour tester les valeurs des arguments
            $this->router->pattern('id', '[1-9]\d*');
            $this->router->pattern('slug', '[a-z-1-9]{3,}');

            //Pages générales :
            $this->router->get('/home',             [ PageController::class, 'home' ]);
            $this->router->get('/mentions-legales', [ PageController::class, 'legalNotice' ]);
            $this->router->get('/deconnexion',      [ PageController::class, 'deconnexion']);
            $this->router->get('/login',            [ PageController::class, 'login']);
            $this->router->get('/register',         [ PageController::class, 'register']);

            //Inscription et connexion utilisateur :
            $this->router->post('/register-user',   [ UserController::class, 'registerNewUser' ]);
            $this->router->post('/connexion-user',  [ UserController::class, 'checkConnexionUser']);

            //Voir le profil
            $this->router->get('/mon-profil',       [ UserController::class, 'showProfile' ]);

            //Voir toutes les locations :
            $this->router->get('/',                 [ RentalController::class, 'findAll']);
            $this->router->get('/locations',        [ RentalController::class, 'findAll']);

            //voir un logement en détail
            $this->router->get('/hebergement/{id}',   [ RentalController::class, 'showDetails' ]);
            $this->router->get('/hebergement/{slug}', [ RentalController::class, 'showBySlug' ]);

            //Ajouter un logement (formulaire) et enregistrer les infos
            $this->router->get('/ajouter-un-logement',    [ RentalController::class, 'displayNewRentalForm' ]);
            $this->router->post('/register-informations', [ RentalController::class, 'registerNewRental' ]);

            //Afficher les réservations - faire une réservation:
            $this->router->get('/mes-reservations', [ BookingsController::class, 'showBookings' ]);
            $this->router->post('/new-booking',     [ BookingsController::class, 'newBooking']);

        }

        private function startRouter() : void
        {
            try {
                $this->router->dispatch();
            }
            catch (RouteNotFoundException $e) {
                View::renderError();
            }
            catch ( InvalidCallableException $e) {
                View::renderError(500);
                echo 'He he, vous n\'avez pas dit le mot magique ^^\'';
            }
        }

    }