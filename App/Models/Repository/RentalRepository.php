<?php

namespace App\Models\Repository;

    use App\AppRepoManager;
    use App\Models\Repository\AddressRepository;
    use App\Models\Address;
    use App\Models\Rental;
    use LidemCore\Repository;
    use LidemCore\GlobalFunctions;
    use LidemCore\View;

    class RentalRepository extends Repository
{
    /**
     * Retourne le nom de la table : 'rentals'
     * @return string
     */
    public function getTableName() : string {return 'rentals';}


    /**
     * Affiche l'ensemble des locations (toutes pour le voyageur et seulement
     * celles de l'annonceur en fonction de l'$user->mode dans la session)
     * @return array|null
     *
     */
    public function findAll() : ?array
{
    global $user;

    $owner_clause = '';
    if($user->type=== HOST) {
        $owner_clause = ' WHERE user_id=:id';
    }
    $result=[];

    $q = sprintf('SELECT 
            `%1$s`.id, surface, capacity, price, description, type, user_id, title, 
            localisation.city, localisation.country,
            place_types.label as type
        FROM `%1$s`
        JOIN `%2$s` as localisation ON localisation.id = `%1$s`.rental_address_id 
        JOIN `%3$s` on `%3$s`.id = `%1$s`.type ',
        $this->getTableName(),
        AppRepoManager::getRM()->getAddressRepository()->getTableName(),
        AppRepoManager::getRM()->getPlaceRepository()->getTableName()
        );

    $q .= $owner_clause;

    $sth = $this->pdo->prepare($q);
    if(!$sth) return null;

    $sth->execute(['id' => $user->id]);
    while($row_data = $sth->fetch()) $result[] = new Rental($row_data);
    return $result;
    }


    /**
     * retourne un hébergement avec toutes les infos, y compris celles de l'adresse.
     * Pour l'affichage de la page détail d'un logement
     * @return Rental|null
     */
    public function findCompleteRentalById( int $id) :?Rental
    {
        global $user;
        $owner_clause = '';
        $params = $user->type === HOST ? ['id' => $id, 'user_id' => $user->id] : ['id' => $id];
        //Si l'utilisateur connecté est propriétaire, on ne veut pas qu'il accède aux annonces qui ne sont pas les siennes
        if($user->type === HOST) $owner_clause = sprintf('AND `%s`.user_id=:user_id', $this->getTableName());
        //Requête pour récupérer les données du rental + address
        $q = sprintf('SELECT `%1$s`.id, surface, capacity, price, description, type, user_id, title, 
                localisation.city, localisation.country,
                `%2$s`.label as type
            FROM `%1$s`
            JOIN `%3$s` as localisation 
            ON localisation.id = `%1$s`.rental_address_id
            JOIN `%2$s` on `%2$s`.id = `%1$s`.type
            WHERE `%1$s`.id=:id ',
            $this->getTableName(),
            AppRepoManager::getRM()->getPlaceRepository()->getTableName(),
            AppRepoManager::getRM()->getAddressRepository()->getTableName());

        $q .= $owner_clause;

        $sth = $this->pdo->prepare($q);
        if(!$sth) return null;

        $sth->execute($params);
        $row_data = $sth->fetch();

        if(!$row_data) return null;
        else
        {
            $rental = new Rental($row_data);
            $rental->address = new Address(($row_data));
        }

        //Requête 2 : Requête qui récupère la liste des équipements (array)
        $equipments = AppRepoManager::getRM()->getEquipmentRepository()->equipmentForOneRental($id);

        $rental->equipments = $equipments;
        return $rental;
    }


    /**
     * @param string $slug
     * @return Rental|null
     */
    public function findBySlug(string $slug) :?Rental
{
    $q = sprintf('SELECT * FROM `%s` WHERE slug=:slug', $this->getTableName() );

    $sth = $this->pdo->prepare($q);
    if(!$sth) return null;

    $sth->execute(['slug' => $slug]);
    $row_data = $sth->fetch();

    return !empty($row_data) ? new Rental( $row_data ) : null;
}


    /**
     * Envoie un nouveau Rental + nouvelle adresse + nouveau equipements dans la db et
     * Redirige l'utilisateur vers la liste des locations
     * @param Rental $new_rental
     * @return void
     */
    public function createNewRental(Rental $new_rental) :void
    {
        $slug = GlobalFunctions::toSlug($new_rental->title);
        $q = sprintf('INSERT INTO `%s` (surface, capacity, price, description, type, rental_address_id, user_id, title, slug) 
                         VALUES (:surface, :capacity, :price, :description, :type, :rental_address_id, :user_id, :title, :slug)',
                         $this->getTableName());

        $sth = $this->pdo->prepare($q);

        if(!$sth) {
            View::renderError();
        };

        $sth->execute([
            'surface'           => $new_rental->surface,
            'capacity'          => $new_rental->capacity,
            'price'             => $new_rental->price,
            'description'       => $new_rental->description,
            'type'              => $new_rental->type,
            'rental_address_id' => $new_rental->rental_address_id,
            'user_id'           => $new_rental->user_id,
            'title'             => $new_rental->title,
            'slug'              => $slug]);
    }


    /**
     * Récupère le dernier id enregistré dans la db
     * @return int
     */
    public function lastIdInTableRentals() :int
{
    $q = sprintf('SELECT MAX(id) as id FROM `%s`', 'rentals');
    $sth = $this->pdo->query($q);
    $row_data = $sth->fetch();
    return $row_data['id'];
}



}


/*    Params: 8 Key: Name:
    [8] :surface            paramno=-1 name=[8] ":surface"              is_param=1 param_type=2 Key: Name:
    [9] :capacity           paramno=-1 name=[9] ":capacity"             is_param=1 param_type=2 Key: Name:
    [6] :price              paramno=-1 name=[6] ":price"                is_param=1 param_type=2 Key: Name:
    [12] :description       paramno=-1 name=[12] ":description"         is_param=1 param_type=2 Key: Name:
    [5] :type               paramno=-1 name=[5] ":type"                 is_param=1 param_type=2 Key: Name:
    [18] :rental_address_id paramno=-1 name=[18] ":rental_address_id"   is_param=1 param_type=2 Key: Name:
    [8] :user_id            paramno=-1 name=[8] ":user_id"              is_param=1 param_type=2 Key: Name:
    [6] :title              paramno=-1 name=[6] ":title"                is_param=1 param_type=2              */
