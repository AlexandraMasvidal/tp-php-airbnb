<?php

namespace App\Models\Repository;

use App\AppRepoManager;
use App\Models\Equipements;
use LidemCore\Repository;

class EquipmentRepository extends Repository
{

    protected function getTableName(): string { return 'equipements'; }

    public function showAllEquipments() : array { return $this->readAll(Equipements::class); }

    public function equipmentForOneRental(int $id) : ?array
    {
        $arr = [];
        $q = 'SELECT equipements.id as id, equipements.label as label
                    FROM rentals
                    join rental_equipement on rental_equipement.rental_id = rentals.id
                    join equipements on equipements.id = rental_equipement.equipement_id
                    WHERE rentals.id=:id';
        $sth = $this->pdo->prepare($q);
        if(!$sth) return null;

        $sth->execute(['id' => $id]);
        while($row_data = $sth->fetch()) {
            $arr[]= new Equipements($row_data);
        };
        return $arr;
    }

    public function addRentalsEquipementLine(int $id, array $datas) :?bool
    {
        $q = sprintf('INSERT INTO `%s` (rental_id, equipement_id) VALUES ', 'rental_equipement');

        foreach ($datas as $item) {
            $q .= '(' . $id .', '. $item->id .'),';
        }
        //suppression de la dernière virgule
        $q = substr($q, 0, strlen($q)-1);
        var_dump($q);
        $sth = $this->pdo->prepare($q);
        if($sth) var_dump($sth->errorInfo());
        if(!$sth) return null;
        $sth->execute(['id'=> intval($id)]);
        var_dump($sth->errorInfo());
        $sth->debugDumpParams();
        return true;
    }
}