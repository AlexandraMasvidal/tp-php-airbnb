<?php

namespace App\Models\Repository;

use App\Models\User;

class UserRepository extends \LidemCore\Repository
{

    /**
     * Retourne le nom de la table : users
     * @return string
     */
    public function getTableName(): string { return 'users'; }

    /**
     * Prend une string email en argument et vérifie si elle existe dans la db
     * Si oui, retourne un User avec les données de l'utilisateur
     * @param string $email
     * @return User|null
     */
    public function checkIfMailExists(string $email) : ?User
    {
        //1 - vérifier le mail
        $q = sprintf('SELECT * FROM `%s` WHERE email=:email',$this->getTableName());
        $sth = $this->pdo->prepare($q);

        if(!$sth) return null;
        $sth->execute(['email'=> $email]);

        $user_data = $sth->fetch();

        return $user_data === false ? null : new User($user_data);
    }


    /**
     * Récupère le nouvel user généré à partir du post nettoyé par le user controller et envoie les données dans la db
     * retourne false en cas d'échec de la requête et true dans le cas contraire;
     * @param User $user
     * @return bool
     */
    public function createNewUser(User $user) : bool
    {
        var_dump($user);
        // requête pour envoyer le new user dans la DB
        $q = sprintf('INSERT INTO `%s` (email, pass, type, pseudo) 
                             VALUES (:email, :pass, :type, :pseudo)',$this->getTableName());

        $sth = $this->pdo->prepare($q);
        if(!$sth) return false;
        else {
            $sth->execute([
                'email' => $user->email,
                'pass' => $user->pass,
                'type' => $user->type,
                'pseudo' => $user->pseudo
            ]);
            return true;
        }
    }
}