<?php

namespace App\Models\Repository;

use App\AppRepoManager;
use App\Models\Booking;
use LidemCore\Repository;

class BookingsRepo extends Repository
{

    /**
     * retourne le nom de la table : 'bookings'
     * @return string
     */
    protected function getTableName(): string {return 'bookings';}

    /**
     * Retourne un tableau de l'ensemble des réservations du propriétaire ou du locataire
     * en fonction de l'user->id dans la session
     * @return array|null
     */
    public function findAll() : ?array
    {
        global $user;
        global $user_mode;
        $arr_result = [];

        $user_clause = '';
        if($user_mode === REGISTERED) $user_clause = 'WHERE bookings.user_id=:id';
        if($user_mode === HOST)       $user_clause = 'WHERE rentals.user_id =:id';


        $q = sprintf('SELECT `%1$s`.id, rental_id, `%1$s`.user_id, check_in, check_out, `%2$s`.title as title 
              FROM `%1$s` 
              JOIN `%2$s` on `%2$s`.id = `%1$s`.rental_id ',
            $this->getTableName(),
            AppRepoManager::getRM()->getRentalRepo()->getTableName());
        $q .= $user_clause;

        $sth = $this->pdo->prepare($q);
        if(!$sth) return null;

        $sth->execute(['id' => $user->id]);
        while($row_data = $sth->fetch()){
            $arr_result[] = new Booking($row_data);
        };

        return $arr_result;
    }


    /**
     * Enregistre une nouvelle réservation dans la db
     * Erreur -> retourne false
     * Succès : retourne true et envoie les données dans la db
     * @return void
     */
    public function registerNewBooking (Booking $booking) :bool
    {
        $q = sprintf('INSERT INTO `%s` (rental_id, user_id, check_in, check_out)
                             VALUES (:rental_id, :user_id, :check_in, :check_out)',
                             $this->getTableName());

        $sth = $this->pdo->prepare($q);
        if(!$sth) return false;
        else{
            $sth->execute(
                ['rental_id'    => $booking->rental_id,
                    'user_id'   => $booking->user_id,
                    'check_in'  => $booking->check_in,
                    'check_out' => $booking->check_out
                ]);
            return true;
        }
    }


}