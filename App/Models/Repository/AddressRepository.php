<?php

namespace App\Models\Repository;
use LidemCore\Repository;

class AddressRepository extends Repository
{
    /**
     * retourne le nom de la table : rentals_addresses
     * @return string
     */
    public function getTableName(): string { return 'rentals_addresses'; }

    /**
     * Requête qui vérifie l'existence de la combinaison d'une ville et d'un pays lors de la création
     * d'un nouveau logement. Retourne false ou l'id de l'adresse si elle existe déjà.
     * @param string $city
     * @param string $country
     * @return void
     */
    public function isLocatisationExists(string $city, string $country) :? int
    {
        $q = sprintf('SELECT * FROM `%1$s` 
                            WHERE `%1$s`.city =:city and `%1$s`.country=:country',
            $this->getTableName());

        $sth = $this->pdo->prepare($q);
        if(!$sth) return null;

        $sth->execute(['city' => $city, 'country'=>$country]);
        $row_data = $sth->fetch();

        if(empty($row_data)) return 0;
        else return $row_data['id'];
    }

    /**
     * Insère une nouvelle ligne dans la table des adresses et retourne l'id de cette ligne
     * @param string $city
     * @param string $country
     * @return int|null
     */
    public function createNewLocalisation(string $city, string $country) :?int
    {
        $q = sprintf('INSERT INTO `%1$s` (city,country)
                            VALUES (:city, :country)',
            $this->getTableName());

        $sth = $this->pdo->prepare($q);
        if(!$sth) return null;

        $sth->execute(['city' => $city, 'country'=>$country]);

        //Recherche du plus grand id avant création de l'item et retourne le nouvel id
        $qbis = 'select MAX(id) from rentals_addresses';
        $sth = $this->pdo->query($qbis);

        return intval(($row_data = $sth->fetch())['MAX(id)']) + 1;
    }


}