<?php

namespace App\Models\Repository;
use LidemCore\Repository;

class PlaceRepository extends Repository
{

    /**
     * retourne le nom de la table : place_type (type d'hébergement)
     * @return string
     */
    public function getTableName(): string
    {
        return 'place_types';
    }
}