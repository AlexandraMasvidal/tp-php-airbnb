<?php

namespace App\Models;

class Address extends \LidemCore\Model
{
    public string $city;
    public string $country;
}