<?php

namespace App\Models;

use LidemCore\Model;

class Rental extends Model
{
    //propriétés correspondant aux colonnes de la table - remplies par l'hydrator
    public int $surface;
    public int $capacity;
    public int $rental_address_id;
    public int $user_id;
    public string $description;
    public string $title;
    public float $price;
    public string $type;

    //propriétés de modélisation
    public string $city;
    public string $country;
    public ?Address $address = null;
    public array $equipments = [];

}