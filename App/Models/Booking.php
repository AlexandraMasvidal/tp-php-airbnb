<?php

namespace App\Models;

use LidemCore\Model;

class Booking extends Model
{
    public int $id;
    public int $user_id;
    public int $rental_id;
    public string $check_in;
    public string $check_out;
    public string $title;
}