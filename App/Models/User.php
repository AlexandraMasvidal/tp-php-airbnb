<?php

namespace App\Models;

use LidemCore\Model;

class User extends Model
{
    public int $id;
    public int $type;
    public string $email;
    public string $pass;
    public string $pseudo;
}