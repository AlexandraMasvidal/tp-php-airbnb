<?php

namespace App\Models;
use LidemCore\Model;

class ErrorMess extends Model
{
    protected string $empty_field = 'Merci de renseigner ce champs';
    private string $pseudo_syntax = 'Le pseudo ne doit contenir que des chiffres et des lettres (entre 5 et 10 caractères)';
    private string $invalid_mail = 'Mail invalide';
    private string $regex_pass = 'Le mot de passe doit comporter au minimum 5 caractères';

    public function __construct(array $data_row = [])
    {
        parent::__construct($data_row);
    }

    public function displayEmptyFieldError() : string{
        return $this->empty_field;
    }
}