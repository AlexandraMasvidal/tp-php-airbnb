<?php

namespace App\Controller;

use App\AppRepoManager;
use App\Models\User;
use LidemCore\View;

class UserController
{
    /**
     * Inscription d'un nouvel utilisateur : contrôle des données et redirection vers l'accueil pour se connecter
     * @return void
     */
    public function registerNewUser(): void
    {
        $_SESSION['form-error'] = '';
        $sth_is_wrong = false;

        $datas = $_POST;
        $type = intval($datas['type']);
        $pseudo = $datas['pseudo'];
        $email = $datas['email'];
        $emailbis = $datas['emailbis'];
        $password = $datas['pass'];
        $passwordBis = $datas['check-pass'];


        //si un champ est vide on retourne sur le form avec un message d'erreur:
        foreach ($datas as $key => $value) {
            if (empty($value)) {
                $sth_is_wrong = true;
                $_SESSION['form-error'] .= 'Merci de remplir tous les champs <br>';
                break;
            };
        }

        //Vérification des données :
        //Type d'utilisateur
        if($type !== HOST && $type !== REGISTERED) {
            $_SESSION['form-error'] .= 'Merci de sélectionner votre type de compte <br>';
            $sth_is_wrong = true;
        }

        //Pseudo
        $regex_pseudo = '/[a-zA-Z1-9]{5,10}/';
        if (preg_match($regex_pseudo, $pseudo) !== 1) {
            $_SESSION['form-error'] .= 'le pseudo ne doit contenir que des chiffres et des lettres (entre 5 et 10 caractères) <br>';
            $sth_is_wrong = true;
        }
        //Email
            //check si la synthaxe est bonne
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['form-error'] .= 'Mail invalide <br>';
            $sth_is_wrong = true;
        }
            //check si les deux inputs mails correspondent
        if ($email !== $emailbis) {
            $_SESSION['form-error'] .= 'Les mails doivent être identiques <br>';
            $sth_is_wrong = true;
        }
            //check si le mail existe déjà dans la DB:
        $is_mail_already_in_db = AppRepoManager::getRM()->getUserRepository()->checkIfMailExists($email);

        if ($is_mail_already_in_db !== null) {
            $_SESSION['form-error'] .= 'Cet utilisateur existe déjà <br>';
            $sth_is_wrong = true;
        }
        //Mdp
        if (strlen($password) < 5) {
            $_SESSION['form-error'] .= 'Le mot de passe doit comporter au minimum 5 caractères <br>';
            $sth_is_wrong = true;
        }
        if ($password !== $passwordBis) {
            $_SESSION['form-error'] .= 'Les mots de passe doivent être identiques <br>';
            $sth_is_wrong = true;
        }

        //S'il y a eu une ou plusieurs erreurs sur les vérifications, on redirige vers le formulaire qui affiche
        //la liste des erreurs stockées dans $_SESSION['form-error']
        if ($sth_is_wrong) {
            header('Location: /register');
            exit();
        }
        //Si tous les inputs sont bien remplis, on appelle la fonction pour créer un nouvel user dans la bdd

        $new_user = new User($datas);

        if (!AppRepoManager::getRM()->getUserRepository()->createNewUser($new_user)) {
            View::renderError();
        }
        else {
            $_SESSION['form-error'] .= '';
            $_SESSION['user'] = $new_user;
            $_SESSION['user-msg'] = 'Bravo ! Vous êtes maintenant membre de notre secte, veuillez vous connecter pour profiter de tous nos services dès maintenant !!!';
            header('Location: /login');
            exit();
        }

    }

    /**
     * Connexion d'un utilisateur : vérification des données et redirection vers page des locations
     * @return void
     */
    public function checkConnexionUser(): void
    {
        $datas = $_POST;
        $email = $datas['email'];
        $password = $datas['pass'];

        //Test du mail :
        $new_user = AppRepoManager::getRM()->getUserRepository()->checkIfMailExists($email);
        //retourne null si le mail ne matche pas avec la BDD (donc s'il est vide) ou un user dans le cas contraire
        if (is_null($new_user)) {
            $_SESSION['form-error'] .= 'email ou mot de passe incorrect';
            header('Location: /login');
            exit();
        } else {
            //Si le mail existe, Test du password depuis le user créé :
            $pass_test = $password === $new_user->pass;
            if ($pass_test === false) {
                $_SESSION['form-error'] .= 'mot de passe incorrect';
                header('Location: /login');
                exit();
            } else {
                //si pas de problème lors du log, redirection vers page d'accueil location et stockage d'un user dans la session
                //sans son mdp
                $new_user->pass = '';
                $_SESSION['user'] = $new_user;
                $_SESSION['user-mode'] = $new_user->type;
                header('Location: /locations');
            }

        }

    }

    /**
     * Affiche l'ensemble des informations de l'utilisateur
     * @return void
     */
    public function showProfile() : void
    {
        $view = new View('pages/profil');
        $view->title = 'Mon Profil';
        $view->render();
    }
}