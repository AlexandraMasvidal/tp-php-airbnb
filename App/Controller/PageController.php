<?php

namespace App\Controller;
use App\AppRepoManager;
use App\Models\User;
use LidemCore\View;

class PageController
{
    public function home() : void  //index : accueil pour les visiteurs non inscrits
    {
        global $user;
        //S'il n'y a pas d'user enregistré dans la session, on affiche la page d'accueil avec formulaires
        if(empty($user) || !isset($user->id))
        {
            $view = new View('pages/home', true);
            $view->render();
        }
        //S'il y a un user logué, on lui affiche directement la liste des locations
        else
        {
            header('Location: /locations');
        }
    }

    public function login() :void
    {
        $view = new View('pages/login', true);
        $view->render();
    }

    public function register() :void
    {
        $view = new View('pages/register-new-user', true);
        $view->render();
    }

    public function legalNotice() : void
    {
        $view = new View('pages/legal-notice');
        $view->title = 'Mentions anticonstitutionnelles';

        $view->render();
    }



    public function displayMessage() : void
    {
        $view = new View('pages/thank-you');
        $view->title = 'Merci :)';
        $view->render();
        var_dump($_POST);
    }

    public function deconnexion () : void
    {
        session_destroy();
        header('Location: /');
    }
}