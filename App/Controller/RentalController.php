<?php

namespace App\Controller;

use App\AppRepoManager;
use App\Models\Address;
use App\Models\Equipements;
use App\Models\Rental;
use LidemCore\GlobalFunctions;
use LidemCore\View;

class RentalController
{
    public function findAll()
    {
        global $user;
        if(empty($user)) header('Location: /home');
        $view_data = [
            'rentals' => AppRepoManager::getRM()->getRentalRepo()->findAll()
        ];
        $view = new View('rentals/rental-list');
        switch ($_SESSION['user']->type) {
            case HOST:
                $view->title = 'Mes annonces';
                break;
            case REGISTERED:
                $view->title = 'Liste des locations';
                break;
        }
        $view->render($view_data);
    }

    public function showBySlug (string $slug) : void
    {
        $rental_result = AppRepoManager::getRM()->getRentalRepo()->findBySlug($slug);

        if(is_null($rental_result)) {
            View::renderError();
            die; //empêche que le code après le dispatch ne s'exécute (mieux que le return)
        }
        $view_data = [
            'rental' => $rental_result
        ];
        $view = new View('rentals/details');
        $view->title = $rental_result->title;
        $view->render($view_data);
    }

    public function showDetails(int $id) : void
    {

        $rental_result = AppRepoManager::getRM()->getRentalRepo()->findCompleteRentalById($id);

        if(is_null($rental_result)) {
            View::renderError(403);
            die;
        }

        $view_data = [
            'rental' => $rental_result,
        ];

        $view = new View('rentals/details');
        $view->title = $rental_result->title;
        $view->render($view_data);
    }

    /**
     * Va chercher la liste des équipements dans la DB pour les afficher dans le formulaire d'jout d'un
     * nouveau logement.
     * @return void
     */
    public function displayNewRentalForm(): void
    {
        global $user;
        $view_data = [
            'eqpts' => AppRepoManager::getRM()->getEquipmentRepository()->showAllEquipments()
        ];

        $view = new View('rentals/new-rental-form');
        $view->title = "Ajouter un nouvel hébergement";
        $view->render($view_data);
    }

    /**
     * Vérification des données du formulaire.
     * Création d'un objet rental
     * Envoi des données dans la BDD si aucun problème
     * @return void
     */
    public function registerNewRental(): void
    {
        global $user;
        $is_sth_wrong = false;
        $datas = $_POST;


        //TYPE D'HEBERGEMENT:
        $data_type = intval(GlobalFunctions::cleanData($datas['type']));
        if($data_type !== SHARED_ROOM && $data_type !== PRIVATE_ROOM && $data_type !== ENTIRE_PLACE)
        {
            $is_sth_wrong = true;
            $_SESSION['form-error'] .= 'Le type de logement n\'existe pas <br>';
        }
        $datas['type'] = $data_type;

        //CAPACITE
        $data_capacity = intval(GlobalFunctions::cleanData($datas['capacity']));
        if($data_capacity <= 0)
        {
            $is_sth_wrong = true;
            $_SESSION['form-error'] .= 'Le logement doit comprendre au moins un couchage <br>';
        }
        $datas['capacity'] = $data_capacity;

        //SURFACE
        $data_surface = intval(GlobalFunctions::cleanData($datas['surface']));
        if($data_capacity <= 0)
        {
            $is_sth_wrong = true;
            $_SESSION['form-error'] .= 'La superficie du logement doit être supérieure à 0 <br>';
        }
        $datas['surface'] = $data_surface;

        //LOCALISATION
        $data_country = GlobalFunctions::cleanData($datas['country']);
        $data_city = GlobalFunctions::cleanData($datas['city']);

            /*Check si cette adresse existe déjà*/
            $new_rental_address_id = AppRepoManager::getRM()->getAddressRepository()->isLocatisationExists($data_city, $data_country);

            /*Si elle n'exite pas, on l'envoie dans la db :*/
            if($new_rental_address_id === 0) $new_rental_address_id = AppRepoManager::getRM()->getAddressRepository()->createNewLocalisation($data_city, $data_country);
            $datas['rental_address_id'] = $new_rental_address_id;
            $datas['city'] = $data_city;
            $datas['country'] = $data_country;
            $datas['address']= new Address(['id' => $new_rental_address_id, 'city' => $data_city, 'country' => $data_country]);

        //PRIX
        $data_price = intval(GlobalFunctions::cleanData($datas['price']));
        if($data_price <= 0)
        {
            $is_sth_wrong = true;
            $_SESSION['form-error'] .= 'Le prix du logement doit être supérieur à 0 <br>';
        }
        $datas['price'] = $data_price;

        //TITRE
        $data_title = GlobalFunctions::cleanData($datas['title']);
        if(empty($data_title))
        {
            $is_sth_wrong = true;
            $_SESSION['form-error'] .= 'Préciser un titre <br>';
        }
        $datas['title'] = $data_title;

        //DESCRIPTION
        $data_description = GlobalFunctions::cleanData($datas['description']);
        if(empty($data_description))
        {
            $is_sth_wrong = true;
            $_SESSION['form-error'] .= 'Renseigner une description de votre hébergement <br>';
        }
        $datas['description'] = $data_description;

        //EQUIPEMENTS
        //Si des equipements ont été cochés, on les compare à ceux enregistrés dans la DB
        //Si un élément reçu dans le post correspond à la db, on créé un objet equipement que l'on pushe dans
        //le tableau initialisé ci-dessous
        $new_rental_equipments = [];
        if(!empty($datas['equipments']))
        {
            $db_equipments = AppRepoManager::getRM()->getEquipmentRepository()->showAllEquipments();
            foreach ($db_equipments as $item) {
                if(in_array($item->id, $datas['equipments'] )) {
                    $new_rental_equipments[] = new Equipements(['id' => $item->id, 'label' => $item->label]);
                };
            }
        }

        $datas['equipments'] = $new_rental_equipments;
        $datas['user_id'] = $user->id;

        if($is_sth_wrong)
        {
            header('Location: /ajouter-un-logement');
            exit();
        }
        else{
            $newRental = new Rental($datas);

            //Insertion nouveau Rental dans DB
            AppRepoManager::getRM()->getRentalRepo()->createNewRental($newRental);

            //ID du nouveau logement
            $id_new_rental = AppRepoManager::getRM()->getRentalRepo()->lastIdInTableRentals();

            //Insertion Equipements dans DB
            if(count($new_rental_equipments) > 0) AppRepoManager::getRM()->getEquipmentRepository()->addRentalsEquipementLine($id_new_rental, $new_rental_equipments);
                header('Location: /locations');
            exit();
        }

    }

}