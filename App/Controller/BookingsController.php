<?php

namespace App\Controller;

use App\AppRepoManager;
use App\Models\Booking;
use LidemCore\GlobalFunctions;
use LidemCore\View;

class BookingsController
{
    /**
     * Affiche la liste des réservations de l'utilisateur
     * @return void
     */
    public function showBookings() : void
    {
        $view_datas = [
            'bookings' => AppRepoManager::getRM()->getBookingsRepo()->findAll()
        ];
        $view = new View('bookings/booking-list');
        $view->title = 'Mes Réservations';
        $view->render($view_datas);
    }

    /**
     * Enregistrement d'une nouvelle réservation :
     * Contrôle de tous les inputs
     * Renvoie un message d'erreur si les inputs renvoient des valeurs qui ne sont pas des dates
     * Si succès redirige l'utilisateur vers la page des locations avec un message de succès
     * @return void
     */
    public function newBooking() :void
    {
        global $user;
        $datas = $_POST;
        $_SESSION['form-error'] = '';
        $datas['user_id'] = $user->id;

        //CHECK-IN ET CHECK-OUT
        if(GlobalFunctions::dateIsValid($datas['check_in']) === false ||
            GlobalFunctions::dateIsValid($datas['check_in']) === false) {
            $_SESSION['form-error'] = 'Merci de sélectionner des dates valides';
            header('Location: /hebergement/' . $datas['rental_id']);
        }

        else
        {
            $datas['user_id'] = $user->id;
            $new_booking = new Booking($datas);
            $register = AppRepoManager::getRM()->getBookingsRepo()->registerNewBooking($new_booking);
            if($register === false)
            {
                $_SESSION['form-error'] = 'Il y a eu un problème avec la requête';
                header('Location: /hebergement/' . $datas['rental_id']);
                exit();
            }
            else
            {
                header('Location: /locations');
                $_SESSION['form-error'] = 'Merci d\'avoir réservé';
                exit();
            }

        }


    }

}