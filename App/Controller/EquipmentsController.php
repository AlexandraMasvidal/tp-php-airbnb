<?php

namespace App\Controller;

use App\AppRepoManager;
use App\Models\Equipements;
use App\Models\Repository\EquipmentRepository;
use LidemCore\View;

class EquipmentsController
{

    public function displayAllEquipments() : void
    {
        $equipments_list = AppRepoManager::getRM()->getEquipmentRepository()->showAllEquipments();
        $view_data = [
            'list' => $equipments_list,
        ];

        $view = new View('rentals/new-rental-form');
        $view->title = 'Ajouter un logement';
        $view->render($view_data);
    }

}