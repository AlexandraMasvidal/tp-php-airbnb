<?php

namespace App;

use App\Models\Repository\AddressRepository;
use App\Models\Repository\BookingsRepo;
use App\Models\Repository\EquipmentRepository;
use App\Models\Repository\PlaceRepository;
use App\Models\Repository\RegisterRepository;
use App\Models\Repository\RentalRepository;
use App\Models\Repository\UserRepository;
use LidemCore\RepositoryManagerTrait;

class AppRepoManager

{
    use RepositoryManagerTrait;

    private RentalRepository $rentalRepository;
    public function getRentalRepo(): RentalRepository {return $this->rentalRepository;}

    private UserRepository $userRepository;
    public function getUserRepository() : UserRepository {return $this->userRepository;}

    private BookingsRepo $bookingRepository;
    public function getBookingsRepo() : BookingsRepo {return $this->bookingRepository;}

    private EquipmentRepository $equipmentsRepository;
    public function getEquipmentRepository() : EquipmentRepository {return $this->equipmentsRepository;}

    private AddressRepository $addressRepository;
    public function getAddressRepository() : AddressRepository {return $this->addressRepository;}

    private PlaceRepository $placeRepository;
    public function getPlaceRepository() : PlaceRepository {return $this->placeRepository;}


    protected function __construct()
    {
        $config = App::getApp();

        $this->rentalRepository     = new RentalRepository( $config );
        $this->bookingRepository    = new BookingsRepo($config);
        $this->userRepository       = new UserRepository($config);
        $this->equipmentsRepository = new EquipmentRepository($config);
        $this->addressRepository    = new AddressRepository($config);
        $this->placeRepository      = new PlaceRepository($config);
    }
}